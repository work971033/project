import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

    private _url: string = 'https://jsonplaceholder.typicode.com/users';
    courses: any;

    button = false
    buttonLetters = 'Show';


   constructor(private http: HttpClient) {

   }

   ngOnInit() {
        this.http.get(this._url)
        .subscribe(
          courses => this.courses = courses
        )

   }

   show() {
     this.button = !this.button
     if (this.button) {
        this.buttonLetters = 'Hide'
     } else {
        this.buttonLetters = 'Show'
     }
   }
}
